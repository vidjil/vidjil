
Vidjil is an open-source platform for the analysis of high-throughput sequencing data from lymphocytes, developed and maintained by
the [Bonsai bioinformatics lab](https://cristal.univ-lille.fr/bonsai) at CRIStAL (UMR CNRS 9189, Université Lille)
and the [VidjilNet consortium](https://www.vidjil.net) at Inria.

The full list of authors/developers and contributors is found in [doc/credits.md](https://gitlab.inria.fr/vidjil/vidjil/-/blob/dev/doc/credits.md).
